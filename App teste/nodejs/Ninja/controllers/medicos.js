const Medico = require('../models/medico');


module.exports = {


    //get all with Async / Await
    
    index: async function(req, res, next){
        try{
            const medicos = await Medico.find({});
            res.status(200).json(medicos);
        }
        catch(err){
            next(err);
        }  
    },
    
   

    //post new medico with async / await
    newMedico: async (req,res,next) =>{
        try{
            const newMedico = new Medico(req.body);
            const medico = await newMedico.save();
            res.status(201).json(medico);  
        }
        catch(err){
            next(err);
        }
    },


    //Queries by ID
    // Get by ID using async / await
    getMedicoById: async function(req,res,next){
        try{
            const {medicoId} = req.params;
            const medico = await Medico.findById(medicoId);
            res.status(200).json(medico);
        }
        catch(err){
            next(err);
        }    
    },
    

    //Put an medico by id
    replaceMedicoById: async function(req,res,next){
        try{
            const {medicoId} = req.params;
            const newMedico = req.body;
            const result = await Medico.findByIdAndUpdate(medicoId,newMedico);
            const medico = await Medico.findById(medicoId);
            res.status(200).json(medico);
        }
        catch(err){
            next(err);
        }
    }

    
 
}
