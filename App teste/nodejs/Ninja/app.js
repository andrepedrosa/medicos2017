const express = require('express');
const logger = require('morgan');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost/apiprojecttest', { useMongoClient: true });
var uri = 'mongodb://1071205:greceitas2017@ds237475.mlab.com:37475/greceitas';
db = mongoose.connect(uri);
mongoose.set('debug', true);

const app =  express();

//Routes
const medicos = require('./routes/medicos')


//Middlewares
app.use(logger('combined'));
app.use(bodyParser.json());

//Routes
app.use('/medicos', medicos);

//Catch 404 errors and foward them to error handler
app.use(function(req,res,next){
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//Error handler function
app.use(function(err,req,res,next){
    const error = app.get('env') === 'development' ? err : {};
    const status = err.status || 500;
    //Respond to client
    res.status(status).json({
        error:{
            message: error.message
        }
    });
    //Respond to ourselves
    console.error(err);
});




//Start the server
const port = app.get('port') || 3000;
app.listen(port, function(){ 
    console.log('Server is listening on port ${port}');
});